'user strict';
var sql = require('../config/db');

//Task object constructor
var Task = function (task) {
    this.task = task;
    this.status = 200;
    this.created_at = new Date();
};


Task.create_order = function (data, result) {
    var query = "INSERT INTO `orders` (user_id,location,PNR,date,iscoupon_applied,total_price,order_status,exitpoint,coach,total_laguage,total_weight,payment_mode) values  ('" + data.user_id + "', '" + data.location + "', '" + data.PNR + "', '" + data.date + "', '" + data.iscoupon_applied + "', '" + data.total_price + "', '" + data.order_status  + "', '" + data.exitpoint+ "', '" + data.coach+ "', '" + data.total_laguage+ "', '" + data.total_weight+ "', '" + data.payment_mode+"')";
    sql.query(query, function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        }
        else {
            console.log(res.insertId);
            console.log('inserted', res)
            result(null, res);
        }
    });
};


Task.get_all_orders = function (result) {
    sql.getConnection(function (err, connection) {
        console.log('connection is ',connection)
        var query = "SELECT * FROM `orders`;";
        sql.query(query, function (err, res) {
            if (err) {
                console.log("is exist error: ", err);
                result(err, null);
            }
            else {
                console.log("orders response  " + JSON.stringify(res));
                console.log("ordersers response  " + res[1]);
                result(null, res);
                connection.release();
            }
        });
    });
};

Task.getExitPoints = function (data,result) {
    sql.getConnection(function (err, connection) {
        console.log('connection is ',connection)
        var query = "SELECT * FROM `exitpoints` WHERE `stationname`='"+data.stationName+"'";
        sql.query(query, function (err, res) {
            if (err) {
                console.log("is exist error: ", err);
                result(err, null);
            }
            else {
                console.log("orders response  " + JSON.stringify(res));
                result(null, res);
                connection.release();
            }
        });
    });
};

Task.cancelOrder = function (data,result) {
    sql.getConnection(function (err, connection) {
        console.log('connection is ',connection)
        //var query = "SELECT * FROM `exitpoints` WHERE `stationname`='"+data.stationName+"'";
        var query = "UPDATE `orders` SET `order_status`='cancelled' WHERE `order_id`='"+data.orderId+"'"
        console.log('query is ',query)
        sql.query(query, function (err, res) {
            if (err) {
                console.log("is exist error: ", err);
                result(err, null);
            }
            else {
                console.log("orders response  " + JSON.stringify(res));
                result(null, res);
                connection.release();
            }
        });
    });
};

Task.fetchOrder = function (data,result) {
    sql.getConnection(function (err, connection) {
        console.log('connection is ',connection)
        //var query = "SELECT * FROM `exitpoints` WHERE `stationname`='"+data.stationName+"'";
        var query = "SELECT * FROM  `orders` WHERE `order_id`='"+data.orderId+"'"
        console.log('query is ',query)
        sql.query(query, function (err, res) {
            if (err) {
                console.log("is exist error: ", err);
                result(err, null);
            }
            else {
                console.log("orders response  " + JSON.stringify(res));
                result(null, res);
                connection.release();
            }
        });
    });
};

Task.updatePaymentStatus = function (data,result) {
    sql.getConnection(function (err, connection) {
        console.log('connection is ',connection)
        //var query = "SELECT * FROM `exitpoints` WHERE `stationname`='"+data.stationName+"'";
        var query = "UPDATE `orders` SET `payment_mode`='"+data.payStatus+"' WHERE `order_id`='"+data.orderId+"'"
        console.log('query is ',query)
        sql.query(query, function (err, res) {
            if (err) {
                console.log("is exist error: ", err);
                result(err, null);
            }
            else {
                console.log("orders response  " + JSON.stringify(res));
                result(null, res);
                connection.release();
            }
        });
    });
};

module.exports = Task;