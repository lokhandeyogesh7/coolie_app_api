// module.exports = function (app, myDB) {
//     // app.post('/notes', (req, res) => {
//     //     console.log('inside ', myDB)
//     //     const note = { text: req.body.body, title: req.body.title };
//     //     myDB.collection('notes_demo').find().insert(note, (err, result) => {
//     //         if (err) {
//     //             res.send({ 'error': 'An error has occured' });
//     //         } else {
//     //             res.send(result.ops[0]);
//     //         }
//     //     });
//     // });
// }
'use strict';
module.exports = function (app) {
  var registrationcontroller = require('../controller/registrationcontroller');
  var loginController = require('../controller/logincontroller');
  var productController = require('../controller/productcontroller');
  var orderController = require('../controller/OrderController');
  var feedbackController = require('../controller/feedbackcontroller');


  //registration
  app.route('/register')
    .post(registrationcontroller.register_user);


  //login
  app.route('/login')
    .post(loginController.login_user);

  //reserpass
  app.route('/resetPassword')
    .post(loginController.resetPassword);
  
  //login
  app.route('/updateProfile')
    .post(registrationcontroller.update_user);

  //products
  //login
  app.route('/getproducts')
    .get(productController.getProducts);

  //orders
  app.route('/createOrder')
    .post(orderController.create_order);

  app.route('/getOrders')
    .get(orderController.get_all_orders);

  //feedback
  app.route('/sendFeedback')
    .post(feedbackController.send_feedback);

  //getExitpoints
  app.route('/getExitPoints')
    .post(orderController.getExitPoints);

  //delete or cancel order 
  app.route('/cancelorder')
  .post(orderController.cancelOrder);

   //fetchOrder
   app.route('/fetchorder')
   .post(orderController.fetchOrder);

   //markpaid
   app.route('/updatepaystatus')
   .post(orderController.updatePaymentStatus);

   //checckmobile
   app.route('/checkmobile')
   .post(registrationcontroller.checkMobile);

  // app.route('/getAll')
  //   .get(module.getAllUsers);

}