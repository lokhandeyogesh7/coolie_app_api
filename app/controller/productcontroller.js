'use strict';

var Task = require('../models/productmodel');

exports.getProducts = function (req, res) {
    if (!req.body) {
        res.status(400).send({ error: true, message: 'Please provide username and password' });
    } else {
        Task.getProducts(req.body, function (err, task) {
            console.log('registration response', task)
            if (err)
                res.send(err);
            res.status(200).send({ products: task[0],products_details: task[1], status: true });
        });
    }
};
