'use strict';

var Task = require('../models/OrderModel');

exports.create_order = function (req, res) {
    if (!req.body) {
        res.status(400).send({ error: true, message: 'Something Went Wrong' });
    } else {
        Task.create_order(req.body, function (err, task) {
            console.log('order response', task)
            if (err) {
                res.send(err);
            } else {
                res.status(200).send({ message: 'Order Created Successfully !!', order_id: task.insertId, status: true });
            }
        });
    }
};

exports.get_all_orders = function (req, res) {
    Task.get_all_orders(function (err, task) {
        console.log('controller')
        if (err) {
            res.send(err);
            console.log('res', task);
        } else {
            //res.send(task);
            res.status(200).send({ order_data: task, status: true });
        }
    });
};

exports.getExitPoints = function (req, res) {
    Task.getExitPoints(req.body,function (err, task) {
        console.log('controller')
        if (err) {
            res.send(err);
            console.log('res', task);
        } else {
            //res.send(task);
            res.status(200).send({ exit_points: task, status: true });
        }
    });
};

exports.cancelOrder = function (req, res) {
    Task.cancelOrder(req.body,function (err, task) {
        console.log('controller')
        if (err) {
            res.send(err);
            console.log('res', task);
        } else {
            //res.send(task);
           // console.log('afftected row ',JSON.stringify(task).getString('affectedRows'))
            console.log('afftected row ',task.affectedRows)
            if(task.affectedRows==0){
                res.status(200).send({ message: "Unable to Update Order !!", status: false });
            }else{
            res.status(200).send({ message: "Order Status Updated Successfully!!", status: true });
            }
        }
    });
};

exports.fetchOrder = function (req, res) {
    Task.fetchOrder(req.body,function (err, task) {
        console.log('controller')
        if (err) {
            res.send(err);
            console.log('res', task);
        } else {
            //res.send(task);
           // console.log('afftected row ',JSON.stringify(task).getString('affectedRows'))
            console.log('afftected row ',task.affectedRows)
            if(task.affectedRows==0){
                res.status(200).send({ message: "Unable to Update Order !!", status: false });
            }else{
            res.status(200).send({ orderDetails: task[0], status: true,message: "Order Fetched Successfully !!" });
            }
        }
    });
};

exports.updatePaymentStatus = function (req, res) {
    Task.updatePaymentStatus(req.body,function (err, task) {
        console.log('controller')
        if (err) {
            res.send(err);
            console.log('res', task);
        } else {
            //res.send(task);
           // console.log('afftected row ',JSON.stringify(task).getString('affectedRows'))
            console.log('afftected row ',task.affectedRows)
            if(task.affectedRows==0){
                res.status(200).send({ message: "Unable to Update Order !!", status: false });
            }else{
            res.status(200).send({ message: "Order Status Updated Successfully!!", status: true });
            }
        }
    });
};