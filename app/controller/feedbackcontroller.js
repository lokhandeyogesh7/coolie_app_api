'use strict';

var Task = require('../models/feedbackmodel');

exports.send_feedback = function (req, res) {
    if (!req.body) {
        res.status(400).send({ error: true, message: 'Something Went Wrong' });
    } else {
        Task.send_feedback(req.body, function (err, task) {
            console.log('order response', task)
            if (err) {
                res.send(err);
            } else {
                res.status(200).send({ message: 'Order Created Successfully !!', order_id: task.insertId, status: true });
            }
        });
    }
};