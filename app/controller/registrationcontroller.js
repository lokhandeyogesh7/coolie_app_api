'use strict';

var Task = require('../models/registrationmodel');

exports.list_all_tasks = function (req, res) {
  Task.getAllTask(function (err, task) {
    console.log('controller')
    if (err)
      res.send(err);
    console.log('res', task);
    res.send(task);
  });
};

exports.register_user = function (req, res) {
  if (!req.body) {
    res.status(400).send({ error: true, message: 'Please provide username and password' });
  } else {
    Task.register_user(req.body, function (err, task) {
      console.log('registration response', task)
      if (err) {
        res.send(err);
      } else if (task == "403") {
        res.status(403).send({ message: 'This Number is already registered with us', status: true });
      } else {
        res.status(200).send({ message: 'Registered successfully!', login_response: task, status: true });
      }
    });
  }
};

exports.update_user = function (req, res) {
  if (!req.body) {
    res.status(400).send({ error: true, message: 'Please provide username and password' });
  } else {
    Task.update_user(req.body, function (err, task) {
      console.log('registration response', task)
      if (err) {
        res.send(err);
        console.log('res', task);
      } else {
        //res.send(task);
        // console.log('afftected row ',JSON.stringify(task).getString('affectedRows'))
        console.log('afftected row ', task.affectedRows)
        if (task.affectedRows == 0) {
          res.status(200).send({ message: "Unable to Update Profile !!", status: false });
        } else {
          res.status(200).send({login_response: task[1][0], message: "Profile Updated Successfully!!", status: true });
        }
      }
    });
  }
};

exports.checkMobile = function (req, res) {
  if (!req.body) {
    res.status(200).send({ error: true, message: 'Please Enter Mobile Number' });
  } else {
    Task.checkMobile(req.body, function (err, task) {
      console.log('registration response', task)
      if (err) {
        res.send(err);
      } else if (task == "403") {
        res.status(200).send({ message: 'This Number is already registered with us', status: true });
      } else {
        res.status(200).send({ message: 'This Number is already registered with us.', login_response: task[0], status: true });
      }
    });
  }
};

// exports.getAllUsers = function (req, res) {
//   if (!req.body) {
//     res.status(400).send({ error: true, message: 'Please provide username and password' });
//   } else {
//     Task.getAllUsers(req.body, function (err, task) {
//       if (err)
//         res.send(err);
//       res.status(200).send({ message: 'Login completed successfully!', status: 'ok' });
//     });
//   }
// };