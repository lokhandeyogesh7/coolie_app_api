'use strict';

var Task = require('../models/loginmodel');

exports.login_user = function (req, res) {
  if (!req.body) {
    res.status(400).send({ error: true, message: 'Please provide username and password' });
  } else {
    Task.login_user(req.body, function (err, task) {
      console.log('registration response', task)
      if (err) {
        res.send(err);
      } else if (task == "403") {
        res.status(403).send({ message: 'Somethig went wrong', status: true });
      } else {
        res.status(200).send({ login_reponse: task,status: true });
      }
    });
  }
};

exports.resetPassword = function (req, res) {
  if (!req.body) {
    res.status(400).send({ error: true, message: 'Please provide username and password' });
  } else {
    Task.resetPassword(req.body, function (err, task) {
      console.log('registration response', task)
      if (err) {
        res.send(err);
        console.log('res', task);
      } else {
        console.log('afftected row ', task.affectedRows)
        if (task.affectedRows == 0) {
          res.status(200).send({ message: "Unable to Update Password !!", status: false });
        } else {
          res.status(200).send({message: "Password Updated Successfully!!", status: true });
        }
      }
    });
  }
};
